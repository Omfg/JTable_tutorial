import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by omfg on 05.12.2016.
 */
public class MainWindow extends JFrame {

    private JScrollPane scrollPane;
    private JList<String> list;
    private TutorListModel model;
    JButton removeBtn;
    JButton addBtn;

    public MainWindow() throws HeadlessException {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(640, 480);
        setResizable(false);

        setLocationRelativeTo(null);
        getContentPane().setLayout(null);


        scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 10, 300, 430);
        getContentPane().add(scrollPane);


        model = new TutorListModel();
        list = new JList<String>(model);//setModel Принимает паараметр listModel model

        scrollPane.setViewportView(list);
        removeBtn = new JButton("Remove");
        removeBtn.setBounds(320,10,90,25);
        getContentPane().add(removeBtn);
        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeEleMent(list.getSelectedValue());
                list.updateUI();

            }
        });
        addBtn = new JButton();
        addBtn.setBounds(320,40,90,25);
        addBtn.setText("Add");
        getContentPane().add(addBtn);
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
String s= JOptionPane.showInputDialog(null,"Enter your string");
model.addElement(s);
list.updateUI();
            }
        });


setVisible(true);
    }

    public void addElements(String s){
        model.addElement(s);


    }
    public void removeElement(int index){
        model.removeEleMent(index);

    }
    public void updateUI(){
        list.updateUI();
    }

}
